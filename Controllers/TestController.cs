﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Http;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using System.Text;
using System.Drawing;
using Helpers;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Controllers
{
    public class TestController : Controller
    {
         private readonly IConfiguration configuration;

        public TestController (IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        [HttpGet]
        [Route("api/mergepdfs")]
        public async Task<IActionResult> MergePdfs()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var enc1252 = Encoding.GetEncoding(1252);

            PdfDocument TargetPDF = new PdfDocument();
            MemoryStream TemporalStream = new MemoryStream();

            List<string> filesDir = new List<string>();
            filesDir.Add("./pdfs/pdf1.pdf");
            filesDir.Add("./pdfs/pdf2.pdf");
            filesDir.Add("./pdfs/pdf3.pdf");
            foreach (var formFile in filesDir)
            {
                PdfDocument PDFToMerge = PdfReader.Open(formFile,PdfDocumentOpenMode.Import);               
                for (int i = 0; i < PDFToMerge.PageCount; i++)
                {
                    TargetPDF.AddPage(PDFToMerge.Pages[i]);
                }
                await TemporalStream.FlushAsync();
            }
            TargetPDF.Save(TemporalStream, false);

            //string html = System.IO.File.ReadAllText("pdf_tpl.html");
            // PdfDocument pdf = PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4, 20);
            //pdf.Save("pdf100.pdf");
            /* 
            PdfDocument pdf = PdfReader.Open("pdf1.pdf",PdfDocumentOpenMode.Import);  
            PdfPage pdfPage =pdf.Pages[0];
            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            XFont font = new XFont("Verdana", 20, XFontStyle.Bold);
            graph.DrawString("This is my first PDF document", font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.Center);
            string pdfFilename = "pdf_1_1.pdf";
            pdf.Save(pdfFilename);
            */
            /*
            foreach (var formFile in files)
            {
                await formFile.CopyToAsync(TemporalStream);
                using (PdfDocument PDFToMerge = PdfReader.Open(TemporalStream, PdfDocumentOpenMode.Import))                    
                for (int i = 0; i < PDFToMerge.PageCount; i++)
                {
                    TargetPDF.AddPage(PDFToMerge.Pages[i]);
                }
                await TemporalStream.FlushAsync();
            }
            TargetPDF.Save(TemporalStream, false);
            */

            return new FileStreamResult(TemporalStream, "application/pdf");
        }
        
        [HttpGet]
        [Route("api/sendemail")]
        public async Task<IActionResult> SendEmail()
        {
            //Parsing templates
            Dictionary<string,string>  tags = new Dictionary<string,string> ();
            tags.Add("[[NAME]]","Goos");
            tags.Add("[[URL]]","http://www.foostudio.mx");
            tags.Add("[[ID_AWARD]]","17193817219");
            tags.Add("[[DIRECTION]]","Mariano escobedo de gomez castillo de león 1235, Del Valle, CP 11232");
            string htmlBody = TPLHelper.parseFromFile("./tpl/email_tpl.html",tags);               
            string pdfContent = TPLHelper.parseFromFile("./tpl/pdf_tpl.html",tags);
        
            //Creating PDF from HTML
            string pdfName = "./pdfs/archivo_"+DateTime.Now.ToFileTime()+".pdf";
            await PDFHelper.HtmlToPdf(pdfContent,pdfName);

            //Sending email with PDF Attached
            var from = "gustavo@foostudio.mx";
            var fromName="Gus";
            var subject="Prueba Emailing";
            var to= "gus@foostudio.mx";
            string KEY = "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";
            await MailingHelper.SendEmail(from,fromName,to,subject,htmlBody,pdfName,KEY);

            return StatusCode(200, new {msg="Done"});
        }

        [HttpGet]
        [Route("api/dhl")]
        public string DHLTest()
        {
            long id = 1263755260339599;
            string name = "Gustavo Bertenasco";
            string email= "gustavo@yahoo.com.mx";
            string street = "Mariano Escobedo";
            string streetNumber ="210";
            string interiorNumber="";
            string suburb="Suburbio";
            string municipality="Miguel Hidalgo";
            string state="CDMX";
            string city = "CDMX";
            string phone ="2291093064";
            string cellphone ="2291093064";
            string cp = "03100";
            string reference = "PRUEBAS"; //Address reference

            string fullAddress= StringHelper.RemoveAccents( street.Trim() + " " 
                                                            + streetNumber.Trim() + " " 
                                                            + interiorNumber.Trim() + " " 
                                                            + suburb.Trim() + " " 
                                                            + municipality.Trim() + " " 
                                                            + state.Trim() + " "
                                                            + reference.Trim());
                    
            // Create a consignee using the selected
            DHLConsignee consignee = new DHLConsignee 
            {
                Id = id,
                CompanyName = "--",
                AddressLines = StringHelper.WordWrap(fullAddress, 35, true),
                City = StringHelper.Truncate(StringHelper.RemoveAccents(city), 35),
                PostalCode = cp,
                CountryCode = "MX",
                CountryName = "Mexico",
                Contact = new DHLContact
                {
                    PersonName = StringHelper.RemoveAccents(StringHelper.Truncate(name + " ", 35)) ,
                    PhoneNumber = StringHelper.Truncate(phone, 25),
                    PhoneExtension = "",
                    MobilePhone = StringHelper.Truncate(cellphone, 25),
                    Email = StringHelper.Truncate(email, 50)
                }
            };

            string response="";

            DHLPiece[] pieces= new DHLPiece[]{ 
                new DHLPiece() {Id=0, PackageType="EE", Weight=1, DimWeight=10, Width=5, Height= 1, Depth= 1 },
                new DHLPiece() {Id=0, PackageType="EE", Weight=1, DimWeight=11, Width=5, Height= 1, Depth= 1 },
                new DHLPiece() {Id=0, PackageType="EE", Weight=1, DimWeight=12, Width=5, Height= 1, Depth= 1 }
            };

            DHLConnector.SetConnectionString(configuration["DHL:Server:Endpoint"]); // set the DHLConnector configuration.

            //Searching for the list of products to send the package
            List<DHLProduct> products = DHLConnector.RequestProducts("03100"); // Check if the user's address is eligible for shipment

            //Searching for the list of products to send the package
            List<DHLQtdShp> products2 = DHLConnector.GetQuotes("03100","Ciudad de México","11800",pieces); // Check if the user's address is eligible for shipment
            if (!products2.Any()) return "Address not elegible for shipment. Please verify the selected address or use another one"; 

           
            //A sending method exist
            Console.WriteLine("Yeey, existe al menos una manera de enviarlo.");
            Console.WriteLine("Solicitando envío y guía por método: "+products[0].globalProductCode+"-"+products[0].localProductCode);
            DHLAWBNumber awbnumber = DHLConnector.ProcessShipment(consignee, pieces);
            if(awbnumber == null) return "No se pudo obtener el número de guía";
            response = awbnumber.number;

            /*DHLAWBNumber awbnumber2 =  new DHLAWBNumber(response);
            DHLCheckpoint checkpoint= DHLConnector.RequestTrackingInfo(awbnumber2);
            if ( checkpoint!=null) response = checkpoint.ToString();
            */            
            return response;
        }

        [HttpGet]
        [Route("api/newtask")]
        public string NewTask()
        {
            TaskBuffer.Init(configuration["ConnectionStrings:DefaultConnection"],5);

            //TaskBuffer.BTaskMethod btask = new TaskBuffer.BTaskMethod("Helpers.TaskBuffer","DummyTest", new List<TaskBuffer.BTaskParam>());
            //TaskBuffer.PushTask(btask.GetBTask());
            BTask btask;
            for(var i=0;i<1000;i++){
                btask = new BTask();
                //btask.CreateEndpoint("http://localhost:5000/api/testtask","GET", new List<BTask.BTaskParam>());
                btask.CreateEndpoint("http://foostudio.mx","GET", new List<BTask.BTaskParam>());
                TaskBuffer.PushTask(btask);
            }
            return "Done";
        }

        [HttpGet]
        [Route("api/runtask")]
        public string RunTask()
        {
            TaskBuffer.Init(configuration["ConnectionStrings:DefaultConnection"],5,60);
            TaskBuffer.Run();
            return "Done";
        }

        [HttpGet]
        [Route("api/testtask")]
        public void TestTask()
        {
            //Console.WriteLine("ENDPOINT REACHED");
        }

        [HttpGet]
        [Route("api/wordwrap")]
        public string[] WordWrap()
        {   
            string result =StringHelper.Truncate("aosd",10);
            return StringHelper.WordWrap("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut imperdiet interdum libero quis dignissim. Cras dignissim dignissim nisi, ac vulputate dui laoreet eu. Cras scelerisque aliquam mauris at porttitor. Integer at erat efficitur, condimentum lacus a, viverra felis. Quisque dolor lacus, vulputate non eleifend ac, suscipit sit amet arcu.",35);
        }

        [HttpGet]
        [Route("api/getazurekey")]
        public string GetAzureAccesToken()
        {   
            string container= configuration.GetSection("AzureStorage")["ContainerName"];
            string connectionString= configuration.GetSection("AzureStorage")["ConnectionString"];
            return  AzureHelper.GetSasToken(connectionString,container,AzureHelper.WritePermission);
        }

        [HttpGet]
        [Route("api/uploadstream")]
        public async Task<Boolean> UploadAzureStream()
        {   
            AzureHelper.SetConfiguration(configuration);
            StreamReader reader = new StreamReader(new FileStream(@"./pdfs/archivo_131650884402539677.pdf", FileMode.Open));
            return await AzureHelper.UploadStream("archivo_131650884402539677.pdf",reader.BaseStream);
        }

        [HttpGet]
        [Route("api/downloadstream")]
        [Produces("application/octet-stream")]
        public async Task<FileStreamResult> DownloadAzureStream()
        {   
            //Bajando un archivo del AzureBlob, devuelve null si el archivo no se encuentra
            AzureHelper.SetConfiguration(configuration);
            Stream stream = await AzureHelper.DownloadStream("archivo_131650884402539677.pdf");
            
            /*//Guardarlo a un archivo
            using (var fileStream = new FileStream("./output.pdf", FileMode.CreateNew, FileAccess.Write))
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
            }*/

            //Formateando el stream para enviarlo como respuesta de la api
            if( stream !=null ){
                MemoryStream streamM = (MemoryStream) stream;
                byte[] byteInfo = streamM.ToArray();
                streamM.Write(byteInfo, 0, byteInfo.Length);
                streamM.Position = 0;
                return new FileStreamResult(streamM, "application/pdf");
            }else{
                return null; 
            }
        }
    }
}