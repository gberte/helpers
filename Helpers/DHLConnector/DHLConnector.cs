using System.Net;
using System.Text;
using System.IO;
using System;
using System.Xml;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Helpers
{
class DHLConnector
{
    private static string _endpoint; 

    public static void SetConnectionString(string endpoint){
        _endpoint = endpoint;
    }

    //Call to the endpoint and return the raw data
    private static string _getEndPointResponse(string postData){
        var request = (HttpWebRequest)WebRequest.Create(_endpoint);
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = "POST";
        request.ContentType = "text/html";
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        return new StreamReader(response.GetResponseStream()).ReadToEnd();   
    }

    //Extracts a list of nodes maching the path
    private static XmlNodeList _getNodeFromPath(string xmlString,string path){
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlString);
        XmlNamespaceManager nmspc = new XmlNamespaceManager(doc.NameTable);
        nmspc.AddNamespace("res", "http://www.dhl.com");
        XmlElement root = doc.DocumentElement;
        return root.SelectNodes(path,nmspc);        
    }

    private static XmlNodeList _getServices(string xmlString){
        return _getNodeFromPath(xmlString,"/res:DCTResponse/GetCapabilityResponse/Srvs/Srv");        
    }

    private static XmlNodeList _getQtdShp(string xmlString){
        return _getNodeFromPath(xmlString,"/res:DCTResponse/GetQuoteResponse/BkgDetails/QtdShp");        
    }

    private static XmlNodeList _getAirwayBillNumber(string xmlString){
        return _getNodeFromPath(xmlString,"/res:ShipmentResponse/AirwayBillNumber");        
    }

    private static XmlNodeList _getCheckpoint(string xmlString){
        return _getNodeFromPath(xmlString,"/res:TrackingResponse/AWBInfo/ShipmentInfo/ShipmentEvent/ServiceEvent/EventCode");        
    }

    private static XmlNodeList _getPDF(string xmlString){
        return _getNodeFromPath(xmlString,"/res:ShipmentResponse/LabelImage");        
    }

    private static string _createMessagereference()
    {
        return Guid.NewGuid().ToString().Substring(0,30);
    }

    //Call to the endpoint and return a list of products, if it is empty it means that the piece can't be delivered
    public static List<DHLProduct> RequestProducts(string cp){
        string date = DateTime.Now.ToString("yyyy-MM-dd");
        string messageTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"); ///2018-03-31T09:30:47
        string messageReference = _createMessagereference();
        
        Dictionary<string,string>  tags = new Dictionary<string,string> ();
        tags.Add("[[MessageTime]]",messageTime);
        tags.Add("[[MessageReference]]",messageReference);
        tags.Add("[[Date]]",date);
        tags.Add("[[ToCountryCode]]","MX");
        tags.Add("[[ToPostalcode]]",cp);
        string postData = parseFromFile("./Helpers/DHLConnector/dhl_capability.xml",tags); 

        Console.WriteLine("-- DEBUGGING postData--");
        Console.WriteLine(postData);

        string response= _getEndPointResponse(postData);

        Console.WriteLine("-- DEBUGGING response--");
        Console.WriteLine(response);

        //Parsing the response to get the list of capabilities
        XmlNodeList nodes = _getServices(response);
        List<DHLProduct> results = new List<DHLProduct>();
        foreach (XmlNode node in nodes)
        {
            results.Add(new DHLProduct( node["MrkSrv"]["LocalProductCode"].InnerText,node["GlobalProductCode"].InnerText,node["MrkSrv"]["ProductShortName"].InnerText));
        }
        return results;
    }

    //Call to the endpoint and return a list of products, if it is empty it means that the piece can't be delivered
    public static List<DHLQtdShp> GetQuotes(string cpFrom,  string cityFrom, string cpTo, DHLPiece[] pieces){
        string date = DateTime.Now.ToString("yyyy-MM-dd");
        string messageTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"); ///2018-03-31T09:30:47
        string messageReference = _createMessagereference();
        
        string piecesXML="<Pieces>";
        DHLPiece piece=null;
        long totalWeight=0;
        for (int i=0; i< pieces.Length; i++) 
        {
            piece = pieces[i];
            piecesXML+="<Piece>"
                +"<PieceID>"+piece.Id+"</PieceID>"
                +"<Height>"+piece.Height+"</Height>"
                +"<Depth>"+piece.Depth+"</Depth>"
                +"<Width>"+piece.Width+"</Width>"
                +"<Weight>"+piece.Weight+"</Weight>"
            +"</Piece>";
            totalWeight+=piece.Weight;
        }                
        piecesXML+="</Pieces>";

        Dictionary<string,string>  tags = new Dictionary<string,string> ();
        tags.Add("[[MessageTime]]",messageTime);
        tags.Add("[[MessageReference]]",messageReference);
        tags.Add("[[Date]]",date);
        tags.Add("[[FromCity]]", cityFrom);
        tags.Add("[[FromCp]]", cpFrom);
        tags.Add("[[ToCountryCode]]","MX");
        tags.Add("[[ToPostalcode]]",cpTo);
        tags.Add("[[Pieces]]",piecesXML);
        string postData = parseFromFile("./Helpers/DHLConnector/dhl_quote.xml",tags); 

        Console.WriteLine("-- DEBUGGING postData--");
        Console.WriteLine(postData);

        string response= _getEndPointResponse(postData);

        Console.WriteLine("-- DEBUGGING response--");
        Console.WriteLine(response);

        //Parsing the response to get the list of capabilities
        XmlNodeList nodes = _getQtdShp(response);
        List<DHLQtdShp> results = new List<DHLQtdShp>();
        foreach (XmlNode node in nodes)
        {
            results.Add(
                new DHLQtdShp() {
                    GlobalProductCode=(node["GlobalProductCode"]!=null)? node["GlobalProductCode"].InnerText:"",
                    LocalProductCode=(node["LocalProductCode"]!=null)? node["LocalProductCode"].InnerText:"",
                    ProductShortName=(node["ProductShortName"]!=null)? node["ProductShortName"].InnerText:"",
                    LocalProductName=(node["LocalProductName"]!=null)? node["LocalProductName"].InnerText:"",
                    NetworkTypeCode=(node["NetworkTypeCode"]!=null)? node["NetworkTypeCode"].InnerText:"",
                    POfferedCustAgreement=(node["POfferedCustAgreement"]!=null)? node["POfferedCustAgreement"].InnerText:"",
                    TransInd=(node["TransInd"]!=null)? node["TransInd"].InnerText:"",
                    PickupDate=(node["PickupDate"]!=null)? node["PickupDate"].InnerText:"",
                    PickupCutoffTime=(node["PickupCutoffTime"]!=null)? node["PickupCutoffTime"].InnerText:"",
                    BookingTime=(node["BookingTime"]!=null)? node["BookingTime"].InnerText:"",
                    CurrencyCode=(node["CurrencyCode"]!=null)? node["CurrencyCode"].InnerText:"",
                    ExchangeRate=(node["ExchangeRate"]!=null)? node["ExchangeRate"].InnerText:"",
                    WeightCharge=(node["WeightCharge"]!=null)? node["WeightCharge"].InnerText:"",
                    TotalTransitDays=(node["TotalTransitDays"]!=null)? node["TotalTransitDays"].InnerText:"",
                    PickupPostalLocAddDays=(node["PickupPostalLocAddDays"]!=null)? node["PickupPostalLocAddDays"].InnerText:"",
                    DeliveryPostalLocAddDays=(node["DeliveryPostalLocAddDays"]!=null)? node["DeliveryPostalLocAddDays"].InnerText:"",
                    PickupNonDHLCourierCode=(node["PickupNonDHLCourierCode"]!=null)? node["PickupNonDHLCourierCode"].InnerText:"",
                    DeliveryNonDHLCourierCode=(node["DeliveryNonDHLCourierCode"]!=null)? node["DeliveryNonDHLCourierCode"].InnerText:"",
                    DeliveryDate=(node["DeliveryDate"]!=null)? node["DeliveryDate"].InnerText:"",
                    DeliveryTime=(node["DeliveryTime"]!=null)? node["DeliveryTime"].InnerText:"",
                    DimensionalWeight=(node["DimensionalWeight"]!=null)? node["DimensionalWeight"].InnerText:"",
                    WeightUnit=(node["WeightUnit"]!=null)? node["WeightUnit"].InnerText:"",
                    PickupDayOfWeekNum=(node["PickupDayOfWeekNum"]!=null)? node["PickupDayOfWeekNum"].InnerText:"",
                    DestinationDayOfWeekNum=(node["DestinationDayOfWeekNum"]!=null)? node["DestinationDayOfWeekNum"].InnerText:""
                }
            );
        }
        return results;
    }

    //Call to the endpoint and return the guide number, otherwise return null
    public static DHLAWBNumber ProcessShipment(DHLConsignee consignee, DHLShipper shipper, DHLPiece[] pieces)
    {
        string messageTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"); ///2018-03-31T09:30:47
        string sendDate = DateTime.Now.ToString("yyyy-MM-dd"); ///2018-03-31
        string messageReference = _createMessagereference();
        
        string[] addressLines = consignee.AddressLines;//Each addressline must have less than 35 characters long

        string addressLinesXML ="";
        string line="";
        for (int i=0; i< addressLines.Length && i<3; i++) 
        {
            line = addressLines[i];
            if (!String.IsNullOrEmpty(line)) addressLinesXML += "<AddressLine>"+line+"</AddressLine>";
        };

        string piecesXML="<Pieces>";
        DHLPiece piece=null;
        long totalWeight=0;
        for (int i=0; i< pieces.Length; i++) 
        {
            piece = pieces[i];
            piecesXML+="<Piece>"
                +"<PieceID>"+piece.Id+"</PieceID>"
                +"<PackageType>"+piece.PackageType+"</PackageType>"
                +"<Weight>"+piece.Weight+"</Weight>"
                +"<DimWeight>"+piece.DimWeight+"</DimWeight>"
                +"<Width>"+piece.Width+"</Width>"
                +"<Height>"+piece.Height+"</Height>"
                +"<Depth>"+piece.Depth+"</Depth>"
            +"</Piece>";
            totalWeight+=piece.Weight;
        }                
        piecesXML+="</Pieces>";

        string shipperXML="<Shipper>"+
            "<ShipperID>"+shipper.ID+"</ShipperID>"+
            "<CompanyName>"+shipper.CompanyName+"</CompanyName>"+
            "<RegisteredAccount>"+shipper.RegisteredAccount+"</RegisteredAccount>"+
            "<AddressLine>"+shipper.AddressLine1+"</AddressLine>";
        if(! String.IsNullOrEmpty(shipper.AddressLine2)) shipperXML+= "<AddressLine>"+shipper.AddressLine2+"</AddressLine>";
        if(! String.IsNullOrEmpty(shipper.AddressLine3)) shipperXML+= "<AddressLine>"+shipper.AddressLine3+"</AddressLine>";
        shipperXML+="<City>"+shipper.City +"</City>"+
            "<Division>"+shipper.Division+"</Division>"+
            "<DivisionCode>"+shipper.DivisionCode+"</DivisionCode>"+
            "<PostalCode>"+shipper.PostalCode+"</PostalCode>"+
            "<CountryCode>"+shipper.CountryCode+"</CountryCode>"+
            "<CountryName>"+shipper.CountryName+"</CountryName>"+
            "<Contact>"+
                "<PersonName>"+shipper.Contact.PersonName+"</PersonName>"+
                "<PhoneNumber>"+shipper.Contact.PhoneNumber+"</PhoneNumber>"+
                "<PhoneExtension>"+shipper.Contact.PhoneExtension+"</PhoneExtension>"+
                "<Email>"+shipper.Contact.Email+"</Email>"+
            "</Contact>"+
        "</Shipper>";

        shipperXML+="</Shipper>";

        Dictionary<string,string>  tags = new Dictionary<string,string> ();
        tags.Add("[[MessageTime]]",messageTime);
        tags.Add("[[MessageReference]]",messageReference);
        tags.Add("[[ConCompanyName]]",consignee.CompanyName);
        tags.Add("[[ConPostalCode]]",consignee.PostalCode);
        tags.Add("[[ConCountryCode]]",consignee.CountryCode);
        tags.Add("[[ConCountryName]]",consignee.CountryName);
        tags.Add("[[ConCity]]",consignee.City);
        tags.Add("[[ConPersonName]]",consignee.Contact.PersonName);
        tags.Add("[[ConPhoneNumber]]",consignee.Contact.PhoneNumber);
        tags.Add("[[ConPhoneExtension]]",consignee.Contact.PhoneExtension);
        tags.Add("[[ConEmail]]",consignee.Contact.Email);
        tags.Add("[[ConMobilePhoneNumber]]",consignee.Contact.MobilePhone);
        tags.Add("[[ConReference]]",consignee.Id.ToString());
        tags.Add("[[AddressLines]]",addressLinesXML);
        tags.Add("[[SendDate]]",sendDate);
        tags.Add("[[Pieces]]",piecesXML);
        tags.Add("[[NumberOfPieces]]",pieces.Length.ToString());
        tags.Add("[[Weight]]",totalWeight.ToString());
        tags.Add("[[Shipper]]",shipperXML);
        tags.Add("[[ShipperAccountNumber]]",shipperXML);
        
        string postData = parseFromFile("./Helpers/DHLConnector/dhl_shipment.xml",tags); 

        Console.WriteLine("-- DEBUGGING Shipping postData--");
        Console.WriteLine(postData);

        string response= _getEndPointResponse(postData);

        Console.WriteLine("-- DEBUGGING Shipping response--");
        Console.WriteLine(response);
        
        //Parsing the response to get the guide number  
        XmlNodeList nodes = _getAirwayBillNumber(response);
        if( nodes.Count > 0) {
                //This is not clear, but apparently when ActionStatus appears is because something wrong happens
            if(nodes[0].FirstChild.Name!="ActionStatus"){                    
                string awbnumber = nodes[0].InnerText;
                nodes = _getPDF(response)[0].SelectNodes("OutputImage");
                byte[] bytes = Convert.FromBase64String(nodes[0].InnerText);
                //No errors occured, so the status code should be in the node
                return new DHLAWBNumber(awbnumber, awbnumber+".pdf", response, new MemoryStream(bytes));
            }
        } 
        return null;
    }

    public static DHLCheckpoint RequestTrackingInfo(DHLAWBNumber awbNumber)
    {
        //Call to the endpoint and return the shipping status, otherwise return null
        string messageTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"); ///2018-03-31T09:30:47
        string messageReference = _createMessagereference();

        Dictionary<string,string>  tags = new Dictionary<string,string> ();
        tags.Add("[[MessageTime]]",messageTime);
        tags.Add("[[MessageReference]]",messageReference);
        tags.Add("[[AWBNumber]]",awbNumber.ToString());
        string postData = parseFromFile("./Helpers/DHLConnector/dhl_tracking.xml",tags); 
        
        //Console.WriteLine("-- DEBUGGING Tracking postData--");
        //Console.WriteLine(postData);

        string response= _getEndPointResponse(postData);
        
        //Console.WriteLine("-- DEBUGGING Tracking response--");
        //Console.WriteLine(response);

        //Parsing the response to get the checkpoint
        XmlNodeList nodes = _getCheckpoint(response);
        if( nodes.Count > 0) {
            //No errors occured, so the status code should be in the node
            return new DHLCheckpoint(nodes[0].InnerText);
        }
        return null;
    }

    private static string parseFromFile(string file,Dictionary<string,string> words){
        try{
            string data = System.IO.File.ReadAllText(file);
            foreach( KeyValuePair<string, string> item in words)
            {
                data= data.Replace(item.Key,item.Value);
            }
            return data;
        }catch(Exception e){
            Console.WriteLine(e.Message);
            return "";
        }
    }
}

class DHLProduct
{
    public string localProductCode{ get; set; }
    public string globalProductCode{ get; set; }
    public string productName{ get; set; }

    public DHLProduct(string localProductCode, string globalProductCode, string productName){
        this.globalProductCode = globalProductCode;
        this.localProductCode = localProductCode;
        this.productName = productName;
    }
}
class DHLCheckpoint
{
    private string code;
    public DHLCheckpoint(string code){
        this.code = code;
    }

    public DHLCheckpoint(){
    }

    public string getDescription(){
        Dictionary<string, string> checkpoints = new Dictionary<string, string>();
        //SHIPMENT
        checkpoints.Add("SD","Shipment information received");
        //PROGRESS CHECKPOINT DETAILS
        checkpoints.Add("AD","Agreed Delivery");
        checkpoints.Add("AF","Arrived Facility");
        checkpoints.Add("AR","Arrival in Delivery Facility");
        checkpoints.Add("BL","Bond Location");
        checkpoints.Add("BN","Customer Broker Notified");
        checkpoints.Add("CC","Awaiting Consignee Collection");
        checkpoints.Add("CI","Facility Check In");
        checkpoints.Add("CR","Clearance Release");
        checkpoints.Add("CU","Confirm Uplift");
        checkpoints.Add("DF","Depart Facility");
        checkpoints.Add("ES","Entry Submitted");
        checkpoints.Add("FD","Forwarded Destination - Delivery Details Expected");
        checkpoints.Add("HI","Lodged into Held Inventory Control");
        checkpoints.Add("HO","Lodged out of Held Inventory Control");
        checkpoints.Add("IA","Image Available");
        checkpoints.Add("IC","In Clearance Processing");
        checkpoints.Add("PD","Partial Delivery");
        checkpoints.Add("PL","Processed at Location");
        checkpoints.Add("PU","Shipment Pickup");
        checkpoints.Add("RR","Response Received");
        checkpoints.Add("RW","Weigh and Dimension");
        checkpoints.Add("SA","Shipment Acceptance");
        checkpoints.Add("SI","Shipment Inspection");
        checkpoints.Add("SM","Scheduled for Movement");
        checkpoints.Add("ST","Shipment Intercept");
        checkpoints.Add("TR","Record of Transit");
        checkpoints.Add("WC","With Delivering Courier");

        //COMPLETION CHECKPOINT DETAILS
        checkpoints.Add("BR","Broker Release");
        checkpoints.Add("CS","Closed Shipment");
        checkpoints.Add("DD","Delivered Damaged");
        checkpoints.Add("DS","Destroyed/Disposal");
        checkpoints.Add("OK","Delivery");
        checkpoints.Add("RT","Returned to Consignor");
        checkpoints.Add("SS","Shipment Stopped");
        checkpoints.Add("TP","Forwarded to Third Party–No Delivery Details Expected");

        //SERVICE INCIDENT DETAILS
        checkpoints.Add("BA","Bad Address");
        checkpoints.Add("CA","Closed on Arrival");
        checkpoints.Add("CD","Controllable Clearance Delay");
        checkpoints.Add("CM","Customer Moved");
        checkpoints.Add("DM","Damaged");
        checkpoints.Add("HP","Held for Payment");
        checkpoints.Add("MC","Miscode");
        checkpoints.Add("MD","Missed Delivery Cycle");
        checkpoints.Add("MS","Missort");
        checkpoints.Add("NA","Not Arrived");
        checkpoints.Add("ND","Not Delivered");
        checkpoints.Add("NH","Not Home");
        checkpoints.Add("OH","On Hold");
        checkpoints.Add("RD","Refused Delivery");
        checkpoints.Add("SC","Service Changed");
        checkpoints.Add("TD","Transport Delay");
        checkpoints.Add("TI","Trace Initiated");
        checkpoints.Add("TT","Trace Terminated");
        checkpoints.Add("UD","Uncontrollable Clearance Delay");

        //PICKUP EXCEPTIONS
        checkpoints.Add("FP","Futile Pickup");
        checkpoints.Add("NR","Not Ready");
        checkpoints.Add("MP","Multiple Pick- Up Points");
        checkpoints.Add("DB","Duplicate Booking");
        checkpoints.Add("IR","Incorrect Route");

        string value="";
        if( checkpoints.TryGetValue(this.code,out value) ) return value;
        return null;
    }

    //Maps all the codes to the 3 stages for the app, 0 - Por enviar, 1 - En ruta, 2 - Recibido
    public int getAppStage(){
        List<string> ok = new List<string>(){"OK","CS","TP"};
        List<string> route = new List<string>(){"BR","DD","DS","RT","SS","BA","CA","CD","CM","DM","HP","MC","MD","MS","NA","ND","NH","OH","RD","SC","TD","TI","TT","UD"};
        if(ok.Find(e => e==this.code) != null)  return 2;
        if(route.Find(e => e==this.code) != null)  return 1;
        return 0;
    }

    public string getCode(){
        return this.code;
    }

    public override string ToString()
    {
        return this.getDescription();
    }
}
class DHLAWBNumber{
    public string number{get; set;}
    public string filename{get; set;}
    public string response {get; set;}
    public Stream filecontent{get; set;}

    public DHLAWBNumber(string number, string filename, string response, Stream filecontent){
        this.number = number;
        this.filename= filename;
        this.response = response;
        this.filecontent = filecontent;
    } 

    public DHLAWBNumber(string number){
        this.number = number;
    } 

    public DHLAWBNumber(){
    } 

    public override string ToString()
    {
        return this.number;
    }
}
class DHLConsignee
{
    public long Id {get; set;}
    public string CompanyName {get; set;}
    public string[] AddressLines {get; set;}
    public string City {get; set;}
    public string PostalCode {get; set;}
    public string CountryCode {get; set;}
    public string CountryName {get; set;}
    public DHLContact Contact {get; set;}

    public string fullAddress(){
        string full="";
        string separator="";
        foreach(string line in AddressLines){
            full+= separator + line;
            separator=" ";
        }
        return full;
    }

}

class DHLPiece
{
    public long Id {get; set;}
    public string PackageType {get; set;}
    public long DimWeight {get; set;}
    public int Weight {get; set;}
    public int Width {get; set;}
    public int Height {get; set;}
    public int Depth {get; set;}
}

class DHLContact 
{
    public string PersonName {get; set;}
    public string PhoneNumber {get; set;}
    public string PhoneExtension {get; set;}
    public string Email {get; set;}
    public string MobilePhone {get; set;}
}


class DHLShipper 
{
    public string ID {get; set;}
    public string AccountNumber {get; set;}
    public string RegisteredAccount {get; set;}
    public string CompanyName {get; set;}
    public string AddressLine1 {get; set;}
    public string AddressLine2 {get; set;}
    public string AddressLine3 {get; set;}
    public string City {get; set;}
    public string Division {get; set;}
    public string PostalCode {get; set;}
    public string DivisionCode {get; set;}
    public string CountryCode {get; set;}
    public string CountryName {get; set;}
    public DHLContact Contact {get; set;}
}

class DHLQtdShp 
{
    public string GlobalProductCode {get; set;}
    public string LocalProductCode {get; set;}
    public string ProductShortName {get; set;}
    public string LocalProductName {get; set;}
    public string NetworkTypeCode {get; set;}
    public string POfferedCustAgreement {get; set;}
    public string TransInd {get; set;}
    public string PickupDate {get; set;}
    public string PickupCutoffTime {get; set;}
    public string BookingTime {get; set;}
    public string CurrencyCode {get; set;}
    public string ExchangeRate {get; set;}
    public string WeightCharge {get; set;}
    public string TotalTransitDays {get; set;}
    public string PickupPostalLocAddDays {get; set;}
    public string DeliveryPostalLocAddDays {get; set;}
    public string PickupNonDHLCourierCode {get; set;}
    public string DeliveryNonDHLCourierCode {get; set;}
    public string DeliveryDate {get; set;}
    public string DeliveryTime {get; set;}
    public string DimensionalWeight {get; set;}
    public string WeightUnit {get; set;}
    public string PickupDayOfWeekNum {get; set;}
    public string DestinationDayOfWeekNum {get; set;}
}
}
