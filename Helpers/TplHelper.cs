
using System.Text;
using System.IO;
using System;
using System.Xml;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;

namespace Helpers
{
    class TPLHelper
    {//Getting PDF 
        public static string parseFromFile(string file,Dictionary<string,string> words){
            try{
                string data = System.IO.File.ReadAllText(file);
                foreach( KeyValuePair<string, string> item in words)
                {
                    data= data.Replace(item.Key,item.Value);
                }
                return data;
            }catch(Exception e){
                Console.WriteLine(e);
                return "";
            }
        }
    }
}




