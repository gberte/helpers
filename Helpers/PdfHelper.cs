
using System.Text;
using System.IO;
using System;
using System.Xml;
using System.Collections.Generic;
using SendGrid;
using SendGrid.Helpers;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;


namespace Helpers
{
    class PDFHelper
    {
        private static string pdfConverterEndPoint = "http://138.68.252.18:9090/getpdf"; //Huezo's GO endpoint

        public async static Task<bool> HtmlToPdf(string html, string fileTo){
            try{
                //string postData = System.IO.File.ReadAllText("pdf.html");
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(pdfConverterEndPoint);
                var data = Encoding.Default.GetBytes(html);
                request.Method = "POST";
                request.ContentType = "application/xml";
                request.ContentLength = data.Length;
                request.Connection = "";
                request.Accept = "*/*";
                Stream stream = request.GetRequestStream();
                await stream.WriteAsync(data, 0, data.Length);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream output = File.OpenWrite(@""+fileTo))
                using (Stream input = response.GetResponseStream())
                {
                    input.CopyTo(output);
                }                
            }catch(Exception e){
                Console.WriteLine(e);
            }
            return true;
        }
    }
}




