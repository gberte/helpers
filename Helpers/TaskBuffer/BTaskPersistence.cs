using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Helpers
{
    public class BTaskPersistence{
        
        private string tablename = "taskbuffer";

        private String _connectionString;

        public BTaskPersistence(String connectionString){

            _connectionString = connectionString;

            try{
                //Creating the table if it does not exists
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "CREATE TABLE "+tablename +"("+
                            "id int NOT NULL IDENTITY(1,1),"+
                            "callback text,"+
                            "processed_at datetime,"+
                            "process_id int,"+
                            "processed_time bigint NOT NULL default 0,"+
                            "response nvarchar(1024) NOT NULL default '',"+
                            "created_at datetime NOT NULL,"+
                            "CONSTRAINT PK_"+tablename+" PRIMARY KEY (id))";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand();
                    cmd.CommandText = "CREATE INDEX taskbuffer_processed_at_IDX ON "+tablename +" (processed_at)";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }catch(Exception){
            }
        }
        
        //Find and return a task not assigned to any process yet and assign it to the processid
        public BTask AssignTaskToProcess(int processid, int timeout){
            BTask bTask=null;
            try{
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();                
                    SqlCommand cmd = new SqlCommand();
                    //This query secures that the process gets a task not assigned to any other
                    //or get a task abandoned one minute ago
                    cmd.CommandText = "UPDATE TOP(1) "+tablename+" SET "+
                                "process_id = "+ processid +", "+
                                "processed_at='"+DateTime.Now+"' "+
                                "WHERE process_id IS NULL OR (DATEDIFF(second, processed_at, GETDATE()) >= "+timeout+" AND response ='')";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    int result = cmd.ExecuteNonQuery();
                    if(result>0){
                        SqlDataReader reader;
                        cmd = new SqlCommand();
                        cmd.CommandText = "SELECT id,callback,process_id,processed_at,processed_time,response,created_at "+
                                        "FROM "+tablename+ 
                                        " WHERE process_id ="+processid+" AND response ='' AND processed_time=0";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        reader = cmd.ExecuteReader();
                        if (reader.Read()){
                            bTask= new BTask();
                            bTask.Id = reader.GetInt32(0);
                            bTask.Callback = reader.GetString(1);
                            bTask.ProcessId = reader.GetInt32(2);
                            bTask.ProcessedAt = reader.GetDateTime(3);
                            bTask.ProcessedTime = reader.GetInt64(4);
                            bTask.Response = reader.GetString(5);
                            bTask.CreatedAt = reader.GetDateTime(6);
                        }
                        //Console.WriteLine(bTask.Id);
                        reader.Close();
                    }
                    connection.Close();
                }
            }catch(Exception e){
                Console.WriteLine("AssignTaskToProcess: "+e.Message);
            }
            return bTask;            
        }

        public int TasksToProcess(int timeout){
            try{
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();                
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;
                    cmd = new SqlCommand();
                    cmd.CommandText = "SELECT count(*) FROM "+tablename+" WHERE process_id IS NULL OR (DATEDIFF(second, processed_at, GETDATE()) >= "+timeout+" AND response ='')";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    reader = cmd.ExecuteReader();
                    reader.Close();
                    connection.Close();
                    return reader.RecordsAffected;
                }
            }catch(Exception e){
                Console.WriteLine("ExistTaskToProcess: "+e.Message);
            }
            return 0;            
        }

        public bool Save(BTask task){
            try{
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    if(task.Id==0){
                        cmd.CommandText = "INSERT INTO "+tablename+" (callback,created_at)VALUES('"+ task.Callback.Replace("'", "''") +"','"+ DateTime.Now +"')";
                    }else{
                        cmd.CommandText = "UPDATE "+tablename+" SET "+
                            "callback='"+task.Callback.Replace("'", "''") +"',"+
                            "process_id='"+task.ProcessId +"',"+
                            "processed_at='"+task.ProcessedAt +"',"+
                            "response='"+task.Response +"',"+
                            "processed_time="+task.ProcessedTime+","+
                            "created_at='"+task.CreatedAt+"' "+
                            "WHERE id="+task.Id;
                    }
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                return true;
            }catch(Exception e){
                Console.WriteLine("AssignTaskToProcess: "+e.Message);
                return false;
            }
        }
    }
}
