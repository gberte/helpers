using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Helpers
{
    //This class allows you to execute slow tasks running them asyncronously from a pool of workers
    public static class TaskBuffer
    {
        private static string _connectionString; 
        private static List<Task> _tasksInFlight;
        private static int _maxWorkers;
        private static int _status=0;
        private static int _timeout=0; //in seconds

        private static BTaskPersistence persistence;

        private static string ExecuteTask(BTask btask){
            JObject callback = JsonConvert.DeserializeObject<JObject>(btask.Callback);
            string result;
            if(callback.GetValue("type").ToString()=="endpoint"){                            
                result= CallEndpoint(callback.GetValue("url").ToString(),callback.GetValue("method").ToString(),callback.GetValue("parameters").ToString());
            }else{
                result= CallMethod(callback.GetValue("classname").ToString(),callback.GetValue("method").ToString(),callback.GetValue("parameters").ToString());
            }
            return result;
        }

        private static string CallEndpoint(string url, string method,string postData){
            string result="";
            try{
                var request = (HttpWebRequest)WebRequest.Create(url);
                if(method == "POST"){
                    var data = Encoding.ASCII.GetBytes(postData);
                    request.Method = method;
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;
                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }
                var response = (HttpWebResponse)request.GetResponse();
                result = response.StatusCode.ToString();
            }catch(Exception e){
                result = e.Message;
            }
            return result;
        }

        private static string CallMethod(string classname, string method,string parameters){
            string result ="";
            try{
                Type classs = Type.GetType(classname);
                MethodInfo methodInfo = classs.GetMethod(method);                        
                List<BTask.BTaskParam> paramss = JsonConvert.DeserializeObject<List<BTask.BTaskParam>>(parameters);
                object[] parametersArray = null;
                int count=0;
                if(paramss.Count>0){
                    parametersArray = new object[paramss.Count];
                    foreach(BTask param in parametersArray){
                        parametersArray[count] = param;
                        count++;
                    }
                }
                methodInfo.Invoke(Type.GetType(classname), parametersArray);
                result = "OK";
            }catch(Exception e){
                result = e.Message;
            }
            return result;
        }

        private static Task<bool> RunTask(BTask btask){
            Task<bool> t=Task.Run(()=>{
                if(btask==null) return false;
                try{
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    string result= ExecuteTask(btask);
                    watch.Stop();
                    btask.ProcessedTime =  watch.ElapsedMilliseconds;
                    btask.Response = result;
                    persistence.Save(btask);
                    return true;
                }catch(Exception){
                    return false;
                }
            });
            return t;
        }

        private static Task<bool> Work(int pid){
            BTask bt = persistence.AssignTaskToProcess(pid,_timeout);
            Task<bool> t = RunTask(bt);
            return t;
        }

        public static void Init(string connectionString, int maxWorkers, int timeout){            
            _connectionString = connectionString;
            persistence = new BTaskPersistence(_connectionString);
            _maxWorkers = maxWorkers;
            _timeout = timeout;
            if(_tasksInFlight==null) _tasksInFlight = new List<Task>(maxWorkers);                    
        }

        //Process all the stack
        public static async void Run(){
            if(_maxWorkers==0) throw new Exception("TaskBuffer has been not initialized.");
            _status = 1;
            while(_status==1)
            {
                if(persistence.TasksToProcess(_timeout)>0){ //If exist something to process
                    while(_tasksInFlight.Count == 0){ //If this is not empty it meanse that the TaskBuffer is already running.
                        while (_tasksInFlight.Count < _maxWorkers)
                        {
                            _tasksInFlight.Add(Work(_tasksInFlight.Count));
                        }
                        Task completedTask = await Task.WhenAny(_tasksInFlight).ConfigureAwait(false);                        
                        await completedTask.ConfigureAwait(false);// Propagate exceptions. In-flight tasks will be abandoned if this throws.
                        _tasksInFlight.Remove(completedTask);
                    }
                }
            }            
        }

        //Stops processing the stack and exit.
        public static void Stop(){
            _status = 0;
        }

        //Returns a json with the status information of the TaskBuffer
        public static JObject Status(){
            string json = @"{ Workers: "+ _tasksInFlight.Count + "/"+_maxWorkers+", Tasks: "+ persistence.TasksToProcess(_timeout) +" }";
            JObject report = JObject.Parse(json);
            return report;
        }

        //Add a new task to the stack
        public static bool PushTask(BTask bt){
            try{
                persistence.Save(bt);
                return false;
            }catch(Exception e){
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
