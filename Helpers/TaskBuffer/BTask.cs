using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Helpers
{
    public class BTask{
        public int Id{get; set;}
        public string Callback{get; set;}
        public int ProcessId{get; set;}
        public DateTime ProcessedAt{get; set;}
        public long ProcessedTime{get; set;}
        public string Response{get; set;}
        public DateTime CreatedAt{get; set;} 

        public void CreateMethod(string classname, string method, List<BTaskParam> paramss){
            this.Callback = "{type:'method',classname:'"+classname+"',method:'"+method+"',parameters:"+JsonConvert.SerializeObject(paramss)+"}";
            this.CreatedAt = DateTime.Now;
        }
        public void CreateEndpoint(string url, string method, List<BTaskParam> paramss){
            this.Callback = "{type:'endpoint',url:'"+url+"',method:'"+method+"',parameters:"+JsonConvert.SerializeObject(paramss)+"}";
            this.CreatedAt = DateTime.Now;
        }

        public class BTaskParam{
            public string Type {get; set;}
            public string Value {get; set;}
        }
    }
   
}
