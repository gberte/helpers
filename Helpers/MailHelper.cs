using System.Net;
using System.Text;
using System.IO;
using System;
using System.Xml;
using System.Collections.Generic;
using SendGrid;
using SendGrid.Helpers;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace Helpers
{
    class MailingHelper
    {
        public async static Task<bool> SendEmail(
            string from, 
            string fromName, 
            string to, 
            string subject, 
            string htmlBody, 
            string fileName, 
            string key)
        {
            var msg = new SendGridMessage()
            {
                From =new EmailAddress(from, fromName),
                Subject = subject,
                HtmlContent = htmlBody,
            };
            msg.AddTo(to);
            var bytes = File.ReadAllBytes(fileName);
            var file = Convert.ToBase64String(bytes);

            //Getting file extension
            var parts = fileName.Split(".");
            var extension = parts[parts.Length-1];

            //Attaching the file and send the email
			msg.AddAttachment("ganador."+extension, file);
            var transportWeb = new SendGridClient(key);
            var response = await transportWeb.SendEmailAsync(msg);    
            if (response.StatusCode != HttpStatusCode.Accepted ) return false; 
            else return true;
        }
        
    }
}




