using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Helpers
{
    public static class StringHelper
    {

        public static Dictionary<char, char> accentDictionary = new Dictionary<char, char>
        {
            {'á', 'a'},
            {'é', 'e'},
            {'ó', 'o'},
            {'ú', 'u'},
            {'í', 'i'},
            {'ñ', 'n'},
            {'Á', 'A'},
            {'É', 'E'},
            {'Í', 'I'},
            {'Ó', 'O'},
            {'Ú', 'U'},
            {'Ñ', 'N'}
        };

        public static string[] WordWrap(string text,int max, bool cutWord=false)
        {
            List<string> elems = new List<string>();
            string part="";
            int posSpace=-1;
            int posStart=0;
            while(posStart<text.Length)
            {
                max = (posStart +max < text.Length)? max: text.Length-posStart;
                part = text.Substring(posStart,max);
                posSpace = part.Length;
                if(!cutWord && part.Length==max && posStart+max< text.Length && text.Substring(posStart+max,1) != " "){
                    //Se cortó una palabra y no estoy en el final del texto,  la paso a la siguiente linea
                    posSpace = part.LastIndexOf(' ');
                    if(posSpace <= 0 ) posSpace=max;    
                }
                elems.Add(text.Substring(posStart,posSpace).Trim());
                posStart = posStart+ posSpace;
            }

            return elems.ToArray();   
        }

        public static string Truncate (string text, int max)
        {
            if (string.IsNullOrEmpty(text)) return "";
            if (max <= 0) return text;
            return (max >= text.Length) ? text : text.Substring(0, max);
        }

        public static string RemoveAccents (string accentedString)
        {
            if (string.IsNullOrEmpty(accentedString)) return "";

            foreach (KeyValuePair<char, char> letter in accentDictionary) accentedString = accentedString.Replace(letter.Key, letter.Value);
            return accentedString;
        }
    }
}